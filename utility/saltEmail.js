module.exports = (email) => {

    const rawName = email.split('@')[0];
    const domain = email.split('@')[1];

    const ln = Math.floor(rawName.length / 2);
    const salt = '•'.repeat(ln);

    const processedName = rawName.substr(0, rawName.length - ln) + salt;

    return `${processedName}@${domain}`;
}