//npm modules
const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (resetToken) => {

    let decoded;
    try {
        decoded = jwt.verify(resetToken, config.get('sudoku_jwtPrivateKey'));
    }
    
    catch(_) {
        throw new Error("Invalid reset token");
    }
    
    if(decoded.login) throw new Error("This token can't be used for this operation");

    return decoded;
}