//npm modules
const winston = require('winston');

module.exports = (err) => {

    const logger = winston.createLogger({
        level: 'error',
        format: winston.format.json(),
        defaultMeta: { service: 'user-service' },
        transports: [
      
          new winston.transports.File({ filename: 'error.log', level: 'error' }),
        ]
      });
    logger.error(err.message, err);

    throw new Error("Internal Server Error");
}