//local modules
const authResolver = require('./auth');
const gameResolver = require('./game');

const rootResolver = {
    ...authResolver,
    ...gameResolver
}

module.exports = rootResolver;
