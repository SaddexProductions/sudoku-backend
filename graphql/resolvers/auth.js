//npm modules
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const config = require('config');
const fs = require('fs');
const hb = require('handlebars');
const { promisify } = require('util');

//local modules
const {User, validate, validateReset, validateEditUser} = require('../../models/user');
const Score = require('../../models/score');
const internalError = require('../../utility/internalservererror');
const verifyResetToken = require('../../utility/verifyResetToken');
const saltEmail = require('../../utility/saltEmail');

//setup
const readFileAsync = promisify(fs.readFile);

module.exports = {
    autoLogin: async (_, req) => {

        if (!req.isAuth) return null;

        const user = await User.findById(req.userId);

        if(!user) return null;

        return {
            ...user._doc,
            password: null,
            passwordUpdatedOn: null,
            validResetToken: null
        }

    },
    checkEmail: async ({email}) => {
        const user = await User.findOne({email: email.toLowerCase()});

        return {
            matches: !!user
        }
    },
    checkUsername: async ({username}, req) => {

        const regex = new RegExp(["^", username, "$"].join(""), "i");

        const user = await User.findOne({username: regex});

        if(req.userId && user && req.userId === user._id.toString()) {
            return {
                matches: false
            }
        }

        return {
            matches: !!user
        }
    },
    createUser: async (args) => {

        const {error} = validate(args.userInput);
        if(error) throw new Error(error.details[0].message);

        const regex = new RegExp(["^", args.userInput.username, "$"].join(""), "i");

        const userExisting = await User.findOne({ $or: [{ email: args.userInput.email }, { username: regex }] });

        if (userExisting) return new Error("User already exists");

        if(args.userInput.password.length > 25 || args.userInput.password.length < 6) 
            throw new Error("Password length is outside range 6-25 characters");

        try {

            const password = await bcrypt.hash(args.userInput.password, 12);
            const user = new User({
                username: args.userInput.username,
                email: args.userInput.email.toLowerCase(),
                password
            });
            const result = await user.save();
            return { ...result._doc, password: null, _id: result.id };
        } catch (err) {
            internalError(err);
        }

    },
    checkResetToken: async ({resetToken}) => {

        const decoded = verifyResetToken(resetToken);

        const user = await User.findById(decoded._id);
        if(!user) throw new Error("User does not exist");

        if(user.validResetToken !== resetToken) throw new Error("This token has already been spent");

        return {
            message: "Reset token valid"
        }
    },
    deleteAccount: async ({ password }, req) => {

        if (!req.isAuth) throw new Error("Unauthenticated!");

        const user = await User.findById(req.userId);
        if (!user) throw new Error("User does not exist!");

        const isEqual = await bcrypt.compare(password, user.password);
        if (!isEqual) throw new Error("Invalid password, please try again");

        try {
            await user.remove();

            await Score.deleteMany({ user: req.userId });

            return {
                message: "Account successfully deleted"
            }
        } catch (err) {
            internalError(err);
        }
    },
    editUser: async (args, req) => {

        const {error} = validateEditUser(args.userChange);
        if(error) throw new Error(error.details[0].message);

        if (!req.isAuth) throw new Error("Unauthenticated!");

        const user = await User.findById(req.userId);

        if (!user) throw new Error("User does not exist");

        const isEqual = await bcrypt.compare(args.userChange.password, user.password);
        if (!isEqual) throw new Error("Invalid password");

        if (args.userChange.username) {

            const regex = new RegExp(["^", args.userChange.username, "$"].join(""), "i");

            const existingUser = await User.findOne({ username: regex });

            if (existingUser && req.userId !== existingUser._id.toString()) throw new Error("The new username has already been taken");
        }

        if (args.userChange.newPassword) {

            user.password = await bcrypt.hash(args.userChange.newPassword, 12);
            user.passwordUpdatedOn = new Date().toISOString();
        }

        if(args.userChange.darkMode != null) {
            user.darkMode = args.userChange.darkMode;
        }

        if(args.userChange.defaultSize) {
            user.defaultSize = args.userChange.defaultSize;
        }

        try {
            
            const result = await user.save();

            return {
                token: user.generateAuthToken(true),
                user: {
                    ...result._doc,
                    _id: result.id,
                    password: null,
                    passwordUpdatedOn: null,
                    validResetToken: null
                }
            };
        } catch (err) {
            internalError(err);
        }

    },
    login: async ({ identifier, password }) => {

        //MongoDB username search is normally set as case-insensitive,
        //but not when logging in for security reasons
        const user = await User.findOne({ $or: [{ email: identifier.toLowerCase() }, { username: identifier }] });

        if (!user) throw new Error("User does not exist");

        const isEqual = await bcrypt.compare(password, user.password);
        if (!isEqual) throw new Error("Invalid email/password");

        const token = user.generateAuthToken(true);

        return {
            user: {
                ...user._doc,
                password: null,
                passwordUpdatedOn: null,
                validResetToken: null
            },
            token
        }
    },
    requestPasswordReset: async (args) => {
        
        const user = await User.findOne({ $or: [{ email: args.identifier.toLowerCase() }, { username: args.identifier }] });

        if (!user) throw new Error("User with these credentials not found");

        const token = user.generateAuthToken(false);
        try {

            user.validResetToken = token;
            await user.save();
            let transporter = nodemailer.createTransport({
                host: 'smtp.sendgrid.net',
                port: 465,
                secure: true, // true for 465, false for other ports
                auth: {
                    user: "apikey",
                    pass: config.get('nodemailer_key')
                }
            });
            const html = await readFileAsync('./resources/template.html', { encoding: 'utf-8' });
            let template = hb.compile(html);
            const base = 'https://sudoku.saddexproductions.com';
            let replacements = {
                username: user.username,
                url: `${base}/auth/resetpassword?token=${token}`
            };
            let htmlSend = template(replacements);
            const mailOptions = {
                from: "noreply@saddexproductions.com", // sender address
                to: user.email, // list of receivers
                subject: "Reset password", // Subject line
                html: htmlSend
            }
            transporter.sendMail(mailOptions, (error, info) => {
                if (err) {
                    throw (err);
                }
            });

            return {
                message: `A password reset email was sent to the address ${saltEmail(user.email)}`
            };
        }
        catch (err) {
            internalError(err);
        }
    },
    resetPassword: async ({ newPassword, resetToken }) => {

        const {error} = validateReset({
            newPassword,
            resetToken
        });

        if(error) throw new Error(error.details[0].message);

        const decoded = verifyResetToken(resetToken);

        const user = await User.findById(decoded._id);
        if(!user) throw new Error("User does not exist");

        if(user.validResetToken !== resetToken) throw new Error("This token has already been spent");

        const isSame = await bcrypt.compare(newPassword, user.password);
        if (isSame) throw new Error("The new password can't be the same as the old password");

        try {

            user.password = await bcrypt.hash(newPassword, 12);
            user.validResetToken = null;
            user.passwordUpdatedOn = new Date().toISOString();
            await user.save();

            return {
                message: "Password successfully changed. Redirecting..."
            }
        } catch (err) {
            internalError(err);
        }
    },
    setDarkMode: async ({darkMode}, req) => {
        if (!req.isAuth) throw new Error("Unauthenticated!");

        const user = await User.findById(req.userId);
        if (!user) throw new Error("User does not exist!");

        try {
            user.darkMode = darkMode;
            const result = await user.save();

            return {
                currentSetting: result.darkMode
            }
        } catch(err) {
            internalError(err);
        }

    }
}

