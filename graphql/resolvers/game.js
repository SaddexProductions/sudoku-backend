//npm modules
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const config = require('config');
const arrayEqual = require('../../core/arrayEqual');

//local modules
const Game = require('../../core/game');
const {User} = require('../../models/user');
const Score = require('../../models/score');
const internalError = require('../../utility/internalservererror');
const { encrypt, decrypt } = require('../../utility/cipherDecipher');

module.exports = {
    
    startGame: async (args, req) => {

        if(!req.isAuth) throw new Error("Unauthenticated!");

        if(args.size < 3 || args.size > 6) throw new Error("Grid size is outside range 3-6");
        
        const game = new Game(args.size);

        const metaToken = jwt.sign({
            _id: mongoose.Types.ObjectId(),
            start: new Date().toISOString(),
            end: false,
            size: args.size,
            solution: encrypt(JSON.stringify(game.solution))
        }, config.get('sudoku_jwtPrivateKey'));

        return {
            meta: metaToken,
            board: game.board,
            original: game.board
        }
    },
    endGame: async (args, req) => {
        if(!req.isAuth) throw new Error("Unauthenticated!");

        let decoded;

        try {
            decoded = jwt.verify(args.meta, config.get('sudoku_jwtPrivateKey'));
            decoded.solution = JSON.parse(decrypt(decoded.solution));
        }
        catch (_) {
            throw new Error("Invalid metatoken");
        }

        decoded.end = new Date().toISOString();

        let solutionsEqual = true;
        for(let i = 0; i < decoded.solution.length; i++) {

            if(!arrayEqual(decoded.solution[i], args.solution[i])) solutionsEqual = false;
        }

        if(!solutionsEqual) throw new Error("Invalid solution");

        let score = Math.floor((1000000*(decoded.size**2/9)) - ((new Date(decoded.end) - new Date(decoded.start))/5));

        if(score < 0) score = 1;

        return {
            meta: jwt.sign({
                score,
                user: req.userId,
                _id: decoded._id,
                date: new Date().toISOString(),
                size: decoded.size
            }, config.get('sudoku_jwtPrivateKey')),
            score
        }

    },
    addScore: async (args, req) => {
        if(!req.isAuth) throw new Error("Unauthenticated!");

        let decoded;

        try {
            decoded = jwt.verify(args.gameToken, config.get('sudoku_jwtPrivateKey'));
        }
        catch (_) {
            throw new Error("Invalid result token");
        }

        const count = await Score.countDocuments();
        const lowest = await Score.find().sort("score").limit(1);
        
        if(count >= 2 && lowest.score >= decoded.score) return null;

        const scoreExists = await Score.findById(decoded._id);
        if(scoreExists) throw new Error("Score has already been submitted!");

        try {
            const score = new Score({
                ...decoded
            });
    
            const result = await score.save();
            const {username} = await User.findById(decoded.user);

            const getAll = await Score.find().sort("-score").limit(1000);

            let count = 0;

            for(let i = 0; i < getAll.length; i++){

                count = i + 1;

                if(getAll[i]._id.toString() === result._id.toString()) break;
            }
    
            return {
                score: {
                    ...score._doc,
                    date: new Date(score.date).toISOString(),
                    username
                },
                position: count <= 1000 ? count.toString() : "1000+"
            }
        } catch(err) {
            internalError(err);
        }
    },

    scores: async (args) => {

        if(args.start > 900) throw new Error("Score limit reached");

        const scores = await Score.find().sort("-score").limit(100).skip(args.start);

        return scores.map(async score => {

            const {username} = await User.findById(score.user);

            return {
                ...score._doc,
                username,
                date: new Date(score.date).toISOString()
            };
        });
    }
};