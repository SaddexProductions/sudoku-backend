//npm modules
const jwt = require('jsonwebtoken');
const config = require('config');

//local modules
const {User} = require('../models/user');

module.exports = async (req, _, next) => {
    const token = req.header('x-auth-token');

    if(!token) {
        req.isAuth = false;
        return next();
    }
    try {
        const decoded = jwt.verify(token, config.get('sudoku_jwtPrivateKey'));

        if(!decoded) {
            req.isAuth = false;
            return next();
        } else {

            const user = await User.findById(decoded._id);
            if(!user) {
                return next();
            }

            if(decoded.login && new Date(decoded.signedOn) > new Date(user.passwordUpdatedOn) || decoded.login && !user.passwordUpdatedOn) {
                req.isAuth = true;
                req.userId = decoded._id;
                next();
            } else {
                req.isAuth = false;
                next();
            }
        }
    }
    catch(_) {
        req.isAuth = false;
        return next();
    }
    
}