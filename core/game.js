//npm modules
const {range} = require('lodash');

class Game {

    base;
    board = [];
    nums;
    rBase;
    side;
    solution;

    constructor( base = 3){

        this.base = base;

        this.side = this.base*this.base;
        this.nums = this.shuffle(range(1, this.base*this.base+1));

        this.rBase = range(0, this.base);

        const rows = this.setItems();
        const cols = this.setItems();

        for(let i = 0; i < rows.length; i++) {

            const r = rows[i];
            this.board.push([]);

            for(const c of cols) {

                this.board[i].push(this.nums[this.pattern(r, c)]);
            }
        }

        this.solution = this.board.map((_, index) => [...this.board[index]]);

        const squares = this.side*this.side;
        const empties = Math.floor((squares * 3)/6.5);
        
        const spotsToRemove = this.getRandomSubarray(range(0, squares), empties);
        
        for(const s of spotsToRemove) {
            this.board[Math.floor(s/this.side)][s%this.side] = 0;
        }
    }

     pattern = ( r, c) => {
        return (this.base*(r%this.base)+Math.floor(r/this.base+c)) %this.side;
    }

     shuffle = (inputArray) => {
        for(let i = inputArray.length - 1; i > 0; i--){
            const j = Math.floor(Math.random() * i);
            const temp = inputArray[i]
            inputArray[i] = inputArray[j]
            inputArray[j] = temp
        }

        return inputArray;
    }

    getRandomSubarray(arr, size) {
        var shuffled = arr.slice(0), i = arr.length, temp, index;
        while (i--) {
            index = Math.floor((i + 1) * Math.random());
            temp = shuffled[index];
            shuffled[index] = shuffled[i];
            shuffled[i] = temp;
        }
        return shuffled.slice(0, size);
    }

    setItems() {
        const shuffledArray = this.shuffle([...this.rBase]);
        const output = [];
        for(let i = 0; i < shuffledArray.length; i++) {

            const rc = shuffledArray[i];

            const secondShuffledArray = this.shuffle([...this.rBase]);
            for(let j=0; j < secondShuffledArray.length; j++) {
                const g = secondShuffledArray[j];
                output.push(this.base*g + rc); 
            }
        }
        return output;
    }
}

module.exports = Game;