//npm modules
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('config');
const Joi = require('joi');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        minlength: 3,
        maxlength: 20,
        required: true,
        unique: true
    },
    email: {
        type: String,
        maxlength: 120,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    defaultSize: {
        type: Number,
        default: 3
    },
    darkMode: {
        type: Boolean,
        default: false
    },
    validResetToken: String,
    passwordUpdatedOn: Date
});

UserSchema.methods.generateAuthToken = function (login = false) {
    const token = jwt.sign({
        _id: this._id,
        email: this.email,
        login,
        username: this.username,
        signedOn: new Date()
    }, config.get('sudoku_jwtPrivateKey'));
    return token;
}

const validateUser = (obj) => {
    const Schema = Joi.object({
        email: Joi.string().email().max(120).required(),
        password: Joi.string()
        .regex(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/)
        .required(),
        username: Joi.string().min(3).max(20).required().regex(/^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/)
    });

    return Schema.validate(obj);
}

const validateResetPassword = (obj) => {

    const Schema = Joi.object({
        newPassword: Joi.string()
        .regex(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/)
        .required(),
        resetToken: Joi.string().required()
    });

    return Schema.validate(obj);
}

const validateEditUser = (obj) => {

    const Schema = Joi.object({

        password: Joi.string().required(),
        newPassword: Joi.string()
        .regex(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/),
        darkMode: Joi.boolean(),
        username: Joi.string().min(3).max(20).regex(/^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/),
        defaultSize: Joi.number().min(3).max(6)
    });

    return Schema.validate(obj);
}

exports.validate = validateUser;
exports.validateEditUser = validateEditUser;
exports.validateReset = validateResetPassword;
exports.User = mongoose.model('User', UserSchema);