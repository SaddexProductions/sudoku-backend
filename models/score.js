//npm modules
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ScoreSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    score: {
        type: Number,
        required: true,
        min: 0,
        max: 4000000
    },
    size: {
        type: Number,
        required: true,
        min: 3,
        max: 6
    },
    date: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Score', ScoreSchema);