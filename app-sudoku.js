//npm modules
const express = require('express');
const bodyparser = require('body-parser');
const { graphqlHTTP } = require('express-graphql');
const { buildSchema } = require('graphql');
const mongoose = require('mongoose');
const config = require('config');
const fs = require('fs');
const cors = require('cors'); //dev dependency

//local modules
const gQLSchema = fs.readFileSync('./graphql/schema/schema.graphql', 'utf-8').toString();
const gQLResolver = require('./graphql/resolvers');
const auth = require('./middleware/auth');
const envs = require('./config/default.json');

//config env variable check
const transformedKeys = Object.keys(envs);
for(const key of transformedKeys) {
    if(!config.get(key)) throw new Error(`${key} not defined - please check environment variables`);
}

//express app start
const app = express();

app.use(bodyparser.json());
app.use(cors()); //remove in production
app.use(auth);

//graphQL init
app.use('/api/graphql', graphqlHTTP({
    schema: buildSchema(gQLSchema),
    rootValue: gQLResolver
}));

//mongoDB init
mongoose.connect('mongodb://localhost/Sudoku', {
    useNewUrlParser: true, useUnifiedTopology: true,
    useCreateIndex: true, useFindAndModify: false
}).then(() => {
    app.listen(4000);
}).catch(_ => {
    process.exit(1);
});