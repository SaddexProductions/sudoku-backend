//npm modules
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('config');

//local modules
const {User, validate, validateReset, validateEditUser} = require('../../../models/user');

//setup
const userObj = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    email: "saddexproductions@gmail.com",
    username: "Saddex"
}

const reqObject = {
    email: "saddexproductions@gmail.com",
    password: "t3stP4ssword",
    username: "Saddex"
}

describe("User model", () => {

    it("should return a valid JWT", () => {
        const user = new User(userObj);
        const token = user.generateAuthToken();
        const decoded = jwt.verify(token, config.get("sudoku_jwtPrivateKey"));
        
        expect(decoded).toMatchObject(userObj);
    });

    it("should contain login property in JWT if generate method is called with login set to true", () => {
        const user = new User(userObj);

        const userObj2 = {...userObj, login: true}
        const token = user.generateAuthToken(true);
        const decoded = jwt.verify(token, config.get("sudoku_jwtPrivateKey"));

        expect(decoded).toMatchObject(userObj2);
    })

});

describe('Create user validation', () => {

    it("should validate correctly when all required details are provided", () => {
        const {error} = validate(reqObject)

        expect(error).toBeFalsy();
    })

    it("should generate an error when a required field is not provided", () => {
        const reqObject2 = {...reqObject};
        delete reqObject2.email;

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    });

    it("should generate an error when email isn't in email format", () => {
        const reqObject2 = {...reqObject, email: "dsgsdgg@"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    });

    it("should generate an error when password is too short (less than 6 chars)", () => {
        const reqObject2 = {...reqObject, password: "test"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    });

    it("should generate an error when password is too long (more than 25 chars)", () => {
        const reqObject2 = {...reqObject, password: "testPpppP45pppppppPppPPppppp"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    });

    it("should generate an error when password doesn't contain a number", () => {
        const reqObject2 = {...reqObject, password: "testPpppPppp"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    });

    it("should generate an error when password doesn't contain an uppercase letter", () => {
        const reqObject2 = {...reqObject, password: "test45pppppp"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    });

    it("should generate an error when password doesn't contain an lowercase letter", () => {
        const reqObject2 = {...reqObject, password: "TEST45PPPPPPP"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    });

});

describe('Reset user password validation', () => {

    it("should generate error if token is missing", () => {
        const {error} = validateReset({
            newPassword: "eX4Mpl3"
        });
        expect(error).toBeTruthy();
    });

    it("should validate correctly when all required details are provided", () => {
        const {error} = validateReset({
            resetToken: "exampleToken",
            newPassword: "eX4Mpl3"
        });
        expect(error).toBeFalsy();
    })

    it("should generate an error when password is too short (less than 6 chars)", () => {
        const {error} = validateReset({
            resetToken: "exampleToken",
            newPassword: "test"
        });

        expect(error).toBeTruthy();
    });

    it("should generate an error when password is too long (more than 25 chars)", () => {
        const {error} = validateReset({
            resetToken: "exampleToken",
            newPassword: "testPpppP45pppppppPppPPppppp"
        });

        expect(error).toBeTruthy();
    });

    it("should generate an error when password doesn't contain a number", () => {

        const {error} = validateReset({
            resetToken: "exampleToken",
            newPassword: "testPpppPppp"
        });

        expect(error).toBeTruthy();
    });

    it("should generate an error when password doesn't contain an uppercase letter", () => {

        const {error} = validateReset({
            resetToken: "exampleToken",
            newPassword: "test45fdgffdf"
        });

        expect(error).toBeTruthy();
    });

    it("should generate an error when password doesn't contain an lowercase letter", () => {

        const {error} = validateReset({
            resetToken: "exampleToken",
            newPassword: "TEST45GDGDG"
        });

        expect(error).toBeTruthy();
    });
});

describe('Edit user validation', () => {

    it("should pass if all required fields are there", () => {

        const {error} = validateEditUser({
            password: "example",
            defaultSize: 3
        });

        expect(error).toBeFalsy();
    });
});