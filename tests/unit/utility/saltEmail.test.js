const saltEmail = require('../../../utility/saltEmail');

describe('Email salting function', () => {

    it('should salt the last half of the name in the email address', () => {

        expect(saltEmail('test1234@gmail.com')).toBe('test••••@gmail.com');
    });
});